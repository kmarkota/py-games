import pygame
import random
import  os,sys
from pygame.locals import *
import time


if not pygame.font: print('Warning, fonts disabled')
if not pygame.mixer: print('Warning, sound disabled')

green=(0,155,0)
white= (255,255,255)
black = (0,0,0)
red = (255,0,0)
player_rect=[300,300,10,10]

FPS = 15


display_Width = 800
display_Height = 600


block_size = 10




# apple_rect=[random_X,random_Y,block_size,block_size]
#
pygame.init()
pygame.display.set_caption("Snake")
gameDisplay = pygame.display.set_mode((display_Width,display_Height))


clock = pygame.time.Clock()

# game loop


font = pygame.font.SysFont(None, 25)

def snake(block_size,snakeList):
    for XnY in snakeList:
        pygame.draw.rect(gameDisplay, green, [XnY[0], XnY[1], block_size, block_size], 3)


def text_objects(text,color):
    textSurface = font.render(text,True,color)
    return textSurface, textSurface.get_rect()



def message_to_screen(msg,color):
    textSurf, textRect = text_objects(msg,color)
    # screen_text = font.render(msg,True,color)
    # gameDisplay.blit(screen_text,[display_Width/2,display_Height/2])
    textRect.center = (display_Width/2), (display_Height/2)
    gameDisplay.blit(textSurf,textRect)

def gameLoop():
    gameExit = False
    gameOver = False
    lead_X = display_Width / 2
    lead_Y = display_Height / 2
    lead_X_change = 0
    lead_Y_change = 0
    snakeList = []
    snakeLength=1
    random_X = round(random.randrange(0, display_Width - block_size))#/10)*10
    random_Y = round(random.randrange(0, display_Height - block_size))#/10)*10

    while not gameExit:

        while gameOver==True:
            gameDisplay.fill(white)
            message_to_screen("Game over, press C to play again or Q to quit", red)
            pygame.display.update()

            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    gameExit=True
                    gameOver=False
                if event.type ==pygame.KEYDOWN:
                    if event.key == pygame.K_q:
                        gameExit = True
                        gameOver = False
                    if event.key == pygame.K_c:
                        gameLoop()

        # kretanje
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                gameExit=True
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_LEFT:
                    lead_X_change = -block_size
                    lead_Y_change=0
                elif event.key == pygame.K_RIGHT:
                    lead_X_change = + block_size
                    lead_Y_change=0
                elif event.key == pygame.K_DOWN:
                    lead_Y_change = +block_size
                    lead_X_change=0
                elif event.key == pygame.K_UP:
                    lead_Y_change = -block_size
                    lead_X_change=0

            if event.type == pygame.KEYUP:
                if event.key == pygame.K_RIGHT or event.key == pygame.K_LEFT:
                    lead_X=lead_X+0
                elif event.key == pygame.K_UP or event.key == pygame.K_DOWN:
                    lead_Y =lead_Y + 0

        if lead_X >= display_Width or lead_X <0 or lead_Y <0 or lead_Y >=display_Height :
            gameOver=True

        #      kretanje


        gameDisplay.fill(white)

        lead_X +=lead_X_change
        lead_Y +=lead_Y_change

        apple = pygame.draw.rect(gameDisplay, red, [random_X,random_Y,block_size,block_size], 3)


        snakeHead=[]
        snakeHead.append(lead_X)
        snakeHead.append(lead_Y)
        snakeList.append(snakeHead)

        if len(snakeList) >snakeLength:
            del snakeList[0]

        for eachSegment in snakeList[:-1]:
            if eachSegment == snakeHead:
                gameOver = True

        snake(block_size,snakeList)

        pygame.display.update()

        if lead_X > random_X  and lead_X < random_X + block_size or lead_X + block_size > random_X and lead_X + block_size < random_X + block_size:
            if lead_Y > random_Y and lead_Y < random_Y+block_size:
                random_X = round(random.randrange(0, display_Width - block_size))  # / 10) * 10
                random_Y = round(random.randrange(0, display_Height - block_size))# / 10) * 10
                snakeLength+=1

            elif lead_Y + block_size > random_Y and lead_Y + block_size < random_Y + block_size:
                random_X = round(random.randrange(0, display_Width - block_size))  # / 10) * 10
                random_Y = round(random.randrange(0, display_Height - block_size))  # / 10) * 10
                snakeLength += 1

        # if lead_X >= random_X and lead_X <= random_X+block_size:
        #     if lead_Y >= random_Y and lead_Y <= random_Y + block_size:
        #         random_X = round(random.randrange(0, display_Width - block_size)) #/ 10) * 10
        #         random_Y = round(random.randrange(0, display_Height - block_size))# / 10) * 10
        #         snakeLength+=1



        clock.tick(FPS)


    pygame.quit()
    quit()


gameLoop()